package com.example.laboand2sqlitemerlin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.laboand2sqlitemerlin.Manager.CompteClientManager;
import com.example.laboand2sqlitemerlin.Modeles.CompteClient;

import java.util.ArrayList;

public class MainActivityInformationCompteClient extends AppCompatActivity
{
     private Button mButtonObtenirLesInfoscompteClient;
     private EditText mEditTextidClient;
     private Context context= this;
     private CompteClientManager CompteClientManager1; // objet d'acces à la base de donnee
     private CompteClient CompteClient1;
     private ArrayList<CompteClient> ListeCompte;
     private ArrayAdapter<CompteClient> GestionNaireDesComptes;
     private ListView liste;





    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_information_compte_client);

        //referencons les élements graphiques
        mButtonObtenirLesInfoscompteClient = findViewById(R.id.ButtonObtenirLesInfoscompteClient);
        mEditTextidClient = findViewById(R.id.EditTextidClient);

        //desactivation du bouton au chargement de la page
        mButtonObtenirLesInfoscompteClient.setEnabled(false);

        //activation du bouton
        mEditTextidClient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                mButtonObtenirLesInfoscompteClient.setEnabled(s.toString().length() != 0);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //implementation du bouton Obtenir les informations d'un client quelconque
        mButtonObtenirLesInfoscompteClient.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //recuperation de l'idcomptesaisi
                String idCompteClientSaisi = mEditTextidClient.getText().toString();


               // CompteClient1.setIdClient(idCompteClientSaisi);

                CompteClientManager1 = new CompteClientManager(context);
                CompteClientManager1.open();

                ListeCompte = CompteClientManager1.getCompteClient(idCompteClientSaisi);

               Toast.makeText(getBaseContext(), "le nombre d'éléments retourné est "+ ListeCompte.size(),Toast.LENGTH_LONG).show();
                CompteClientManager1.close();
                finish();
            }
        });





    }


    public void QuitterinformationCompteClient(View view)
    {
        this.finish();
    }
}