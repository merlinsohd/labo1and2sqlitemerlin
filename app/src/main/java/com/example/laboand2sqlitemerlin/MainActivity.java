package com.example.laboand2sqlitemerlin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.laboand2sqlitemerlin.Manager.CompteClientManager;
import com.example.laboand2sqlitemerlin.Modeles.CompteClient;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity
{
     Button mButtonPasserPageCreerCompteClient;
     Button mButtonPasserPageEffacerunComptclient;
     Button mButtonPasserPageModifierrunComptclient;
     Button mButtonPasserPageInfoComptclient;
     Button mButtonPasserPageInfoCompteclientDelinquant;
     Button mButtonPasserPageModifierCreditCompteclient;
     Button mButtonPasserPageModifierSoldeCompteclient;

    private CompteClientManager CompteClientManager1;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Referencons les éléments graphiques
       mButtonPasserPageCreerCompteClient = findViewById(R.id.ButtonPasserPageCreerCompteClient);
       mButtonPasserPageEffacerunComptclient = findViewById(R.id.ButtonPasserPageEffacerunComptclient);
       mButtonPasserPageModifierrunComptclient = findViewById(R.id.ButtonPasserPageModifierrunComptclient);
       mButtonPasserPageInfoComptclient = findViewById(R.id.ButtonPasserPageInfoComptclient);
        mButtonPasserPageInfoCompteclientDelinquant = findViewById(R.id.ButtonPasserPageInfoCompteclientDelinquant);
        mButtonPasserPageModifierCreditCompteclient = findViewById(R.id.ButtonPasserPageModifierCreditCompteclient);
        mButtonPasserPageModifierSoldeCompteclient = findViewById(R.id.ButtonPasserPageModifierSoldeCompteclient);

        // passons à l'activite creer un compte client

        //Passer à l'activite effacer un compte client
        mButtonPasserPageCreerCompteClient.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                passerActiviteCreerUnCompteClient();
            }
        });

           //Passer à l'activite effacer un compte client
        mButtonPasserPageEffacerunComptclient.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                passerActiviteEffacerUnCompteClient();
            }
        });

        //Passer à l'activite Modifier un compte client
        mButtonPasserPageModifierrunComptclient.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                passerActiviteModifierUnCompteClient();
            }
        });

        // creation de la base de donnee et de la table
        CompteClientManager1 = new CompteClientManager(this);
        CompteClientManager1.open();//ouverture de la table en lecture et ecriture
        CompteClientManager1.close();


        // Passer à l'activité Modifier crédit du compte client
        mButtonPasserPageModifierCreditCompteclient.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                PasserActiviteModifierCreditCompteClient();
            }
        });

        //Passer à l'activite modifier le solde d'un client

        mButtonPasserPageModifierSoldeCompteclient.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                PasserActiviteModifierSoldeCompteClient();
            }
        });


        //Passer à l'activite Donner les informations d'un compte client
        mButtonPasserPageInfoComptclient.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                PasserActiviteDonnerInformationCompteClient();
            }
        });




        //PasserActiviteDonnerInformationCompteClient()
    }

       //Methode permettant de passer aux autres activites

      public void passerActiviteCreerUnCompteClient()
      {

              Intent intent = new Intent(this,ActivityCreerUnCompteClient.class);
              startActivity(intent);
              Toast.makeText(this,"Creer un compte client",Toast.LENGTH_SHORT).show();
      }


    public void passerActiviteEffacerUnCompteClient()
    {
        Intent intent = new Intent(this,MainActivityEffacerUnCompteClient.class);
        startActivity(intent);
        Toast.makeText(this,"Effacer un compte client",Toast.LENGTH_SHORT).show();
    }

    public void passerActiviteModifierUnCompteClient()
    {
        Intent intent = new Intent(this,MainActivityModifierUnCompteClient.class);
        startActivity(intent);
        Toast.makeText(this,"Modifier un compte client",Toast.LENGTH_SHORT).show();
    }



    // passer à l'activité Modifier un compteClient

    public void PasserActiviteModifierCreditCompteClient()
    {
        Intent intent = new Intent(this,MainActivityModifierCreditCompte.class);
        startActivity(intent);
        Toast.makeText(this,"Modifier le crédit d'un Compte client",Toast.LENGTH_LONG).show();
    }

      // passer à l'activité Modifier le solde d'un client

   public void PasserActiviteModifierSoldeCompteClient()
    {
        Intent intent = new Intent(this,MainActivityModifierSoldeClient.class);
        startActivity(intent);
        Toast.makeText(this,"Modifier le solde d'un client",Toast.LENGTH_LONG).show();
    }



   public void PasserActiviteDonnerInformationCompteClient()
   {
       Intent intent = new Intent(this,MainActivityInformationCompteClient.class);
       startActivity(intent);

       Toast.makeText(this,"Obtenir les informations d'un compte client",Toast.LENGTH_LONG).show();
   }



}

















