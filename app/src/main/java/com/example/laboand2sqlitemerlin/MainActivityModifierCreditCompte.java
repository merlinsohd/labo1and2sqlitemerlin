package com.example.laboand2sqlitemerlin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.laboand2sqlitemerlin.Manager.CompteClientManager;
import com.example.laboand2sqlitemerlin.Modeles.CompteClient;

public class MainActivityModifierCreditCompte extends AppCompatActivity
{
     private EditText mEditTextidClient;
     private EditText mEditTextcreditClient;
     private Button mButtonModifierUnCompteClient;


     private CompteClient CompteClient1;
     private Context context = this;
     private CompteClientManager CompteClientManager1;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_modifier_credit_compte);

        mEditTextidClient = findViewById(R.id.EditTextidClient);
        mEditTextcreditClient = findViewById(R.id.EditTextcreditClient);
        mButtonModifierUnCompteClient = findViewById(R.id.ButtonModifierUnCompteClient);





        mButtonModifierUnCompteClient.setEnabled(false);

        mEditTextidClient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                mButtonModifierUnCompteClient.setEnabled(s.toString().length() != 0);

            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });


        mButtonModifierUnCompteClient.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                 //recuperation des données
                 String idclientSaisi = mEditTextidClient.getText().toString();
                 float creditClientSaisi = Float.valueOf(mEditTextcreditClient.getText().toString());


                 CompteClient1 = new CompteClient();
                 CompteClient1.setIdClient(idclientSaisi);
                 CompteClient1.setCreditClient(creditClientSaisi);
                 CompteClientManager1 = new CompteClientManager(context);
                 CompteClientManager1.open();
                 int numero = CompteClientManager1.modifierCreditClient(CompteClient1);

                Toast.makeText(getBaseContext(),"Le nombre de ligne modifié est "+ numero,Toast.LENGTH_LONG).show();
                CompteClientManager1.close();
                finish();
            }
        });
    }

    public void QuitterModifierCreditCompteClient(View view)
    {
        this.finish();
    }
}