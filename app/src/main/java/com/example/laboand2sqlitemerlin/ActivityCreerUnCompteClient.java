package com.example.laboand2sqlitemerlin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.laboand2sqlitemerlin.Manager.CompteClientManager;
import com.example.laboand2sqlitemerlin.Modeles.CompteClient;

import java.sql.Date;

public class ActivityCreerUnCompteClient extends AppCompatActivity
{
   private Button mButtonInsererUnCompteClient;
   private  EditText mEditTextidClient;
   private  EditText mEditTextNomClient;
   private  EditText mEditTextPrenomClient;
   private  EditText mEditTextNomUtilisateurClient;
   private  EditText mEditTextmotDePasseClient;
   private  EditText mEditTextadresseclient;
   private  EditText mEditTextsoldeClient;
   private  EditText mEditTextcreditClient;
   private  EditText mEditTextDateOuvertureCompte;

    //creation d'une instance de la base de la connexion à la base de donnee
    private CompteClientManager CompteClientManager1;
    private CompteClient CompteClient1;
    private Context context = this; // on crée un  objet de type context dont on initialise à this


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer_un_compte_client);

        //referençons les éléments graphiques

        mButtonInsererUnCompteClient = findViewById(R.id.ButtonInsererUnCompteClient);
        mEditTextidClient = findViewById(R.id.EditTextidClient);
        mEditTextNomClient = findViewById(R.id.EditTextNomClient);
        mEditTextPrenomClient = findViewById(R.id.EditTextPrenomClient);
        mEditTextNomUtilisateurClient = findViewById(R.id.EditTextNomUtilisateurClient);
        mEditTextmotDePasseClient = findViewById(R.id.EditTextmotDePasseClient);
        mEditTextadresseclient = findViewById(R.id.EditTextadresseclient);
        mEditTextsoldeClient = findViewById(R.id.EditTextsoldeClient);
        mEditTextcreditClient = findViewById(R.id.EditTextcreditClient);
        mEditTextDateOuvertureCompte = findViewById(R.id.EditTextDateOuvertureCompte);

        //desactivons le bouton d'insertion'

        mButtonInsererUnCompteClient.setEnabled(false);

        //activaytion du bouton

      mEditTextidClient.addTextChangedListener(new TextWatcher()
      {
          @Override
          public void beforeTextChanged(CharSequence s, int start, int count, int after)
          {

          }

          @Override
          public void onTextChanged(CharSequence s, int start, int before, int count)
          {
              mButtonInsererUnCompteClient.setEnabled(s.toString().length() != 0);

          }

          @Override
          public void afterTextChanged(Editable s)
          {

          }
      });

        //implementation du bouton d'insertion

        mButtonInsererUnCompteClient.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //recuperation des donnees
                String idclientSaisi = mEditTextidClient.getText().toString();
                String nomclientSaisi = mEditTextNomClient.getText().toString();
                String prenomClientSaisi = mEditTextPrenomClient.getText().toString();
                String adresseClientSaisi = mEditTextadresseclient.getText().toString();
                String nomUtilisateurSaisi = mEditTextNomUtilisateurClient.getText().toString();
                String motDepasseUtilisateurSaisi = mEditTextmotDePasseClient.getText().toString();
                float soldeClioentSaisi = Float.valueOf(mEditTextsoldeClient.getText().toString());
                float CreditClientSaisi = Float.valueOf(mEditTextcreditClient.getText().toString());
                Date DateouvertureSaisi = Date.valueOf(mEditTextDateOuvertureCompte.getText().toString());

                CompteClient1 = new CompteClient();
                CompteClient1.setIdClient(idclientSaisi);
                CompteClient1.setNomClient(nomclientSaisi);
                CompteClient1.setPrenomClient(prenomClientSaisi);
                CompteClient1.setAdresseClient(adresseClientSaisi);
                CompteClient1.setNomUtilisateurClient(nomUtilisateurSaisi);
                CompteClient1.setMotDePasseClient(motDepasseUtilisateurSaisi);
                CompteClient1.setSoldeClient(soldeClioentSaisi);
                CompteClient1.setCreditClient(CreditClientSaisi);
                CompteClient1.setDateOuvertureCompte(DateouvertureSaisi);

                CompteClientManager1 = new CompteClientManager(context);
                CompteClientManager1.open();
                long numeroDeligneInseree = CompteClientManager1.AjouterUnClient(CompteClient1) ;
                Toast.makeText(getBaseContext(),"La ligne numero "+numeroDeligneInseree+" a été inserée ",Toast.LENGTH_LONG).show();
                CompteClientManager1.close();
                finish();
            }
        });

    }


             //Quitter l'activite
            public void QuitterCreerCompteClient(View view)
            {
                this.finish();
            }
   }