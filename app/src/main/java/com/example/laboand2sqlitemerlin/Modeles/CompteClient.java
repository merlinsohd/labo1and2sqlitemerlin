package com.example.laboand2sqlitemerlin.Modeles;

import java.sql.Date;

/**
 * Created by Merlin Sohdjeukeu- Teccart 2020-09-30.
 */
public class CompteClient
{
    //Les attributs
   private String idClient;
   private String nomClient;
   private String PrenomClient;
   private String adresseClient;
   private String nomUtilisateurClient;
   private String motDePasseClient;
   private float soldeClient;
   private float creditClient;
   private java.sql.Date DateOuvertureCompte = new java.sql.Date(0000-00-00);// Date ouverture compte

    public CompteClient()
    {

    }

    //Le constructeur

    public CompteClient(String idClient, String nomClient, String prenomClient, String adresseClient, String nomUtilisateurClient, String motDePasseClient, float soldeClient, float creditClient, Date dateOuvertureCompte)
    {
        this.idClient = idClient;
        this.nomClient = nomClient;
        PrenomClient = prenomClient;
        this.adresseClient = adresseClient;
        this.nomUtilisateurClient = nomUtilisateurClient;
        this.motDePasseClient = motDePasseClient;
        this.soldeClient = soldeClient;
        this.creditClient = creditClient;
        DateOuvertureCompte = dateOuvertureCompte;
    }


    //Les proriétés


    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getNomClient() {
        return nomClient;
    }

    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }

    public String getPrenomClient() {
        return PrenomClient;
    }

    public void setPrenomClient(String prenomClient) {
        PrenomClient = prenomClient;
    }

    public String getAdresseClient() {
        return adresseClient;
    }

    public void setAdresseClient(String adresseClient) {
        this.adresseClient = adresseClient;
    }

    public String getNomUtilisateurClient() {
        return nomUtilisateurClient;
    }

    public void setNomUtilisateurClient(String nomUtilisateurClient) {
        this.nomUtilisateurClient = nomUtilisateurClient;
    }

    public String getMotDePasseClient() {
        return motDePasseClient;
    }

    public void setMotDePasseClient(String motDePasseClient) {
        this.motDePasseClient = motDePasseClient;
    }

    public float getSoldeClient() {
        return soldeClient;
    }

    public void setSoldeClient(float soldeClient) {
        this.soldeClient = soldeClient;
    }

    public float getCreditClient() {
        return creditClient;
    }

    public void setCreditClient(float creditClient) {
        this.creditClient = creditClient;
    }

    public Date getDateOuvertureCompte() {
        return DateOuvertureCompte;
    }

    public void setDateOuvertureCompte(Date dateOuvertureCompte) {
        DateOuvertureCompte = dateOuvertureCompte;
    }
}
