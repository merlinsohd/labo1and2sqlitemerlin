package com.example.laboand2sqlitemerlin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.laboand2sqlitemerlin.Manager.CompteClientManager;
import com.example.laboand2sqlitemerlin.Modeles.CompteClient;

public class MainActivityModifierSoldeClient extends AppCompatActivity
{
   private EditText mEditTextidClient;
   private EditText mEditTextsoldeClient;
   private Button mButtonModifierSoldeClient;

   private Context context = this;
   private CompteClientManager CompteClientManager1; // pour la connectiion à la base de donnee
   private CompteClient CompteClient1;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_modifier_solde_client);

        //referencons les éléments graphiques
        mEditTextidClient = findViewById(R.id.EditTextidClient);
        mEditTextsoldeClient = findViewById(R.id.EditTextsoldeClient);
        mButtonModifierSoldeClient = findViewById(R.id.ButtonModifierSoldeClient);


        mButtonModifierSoldeClient.setEnabled(false);

        mEditTextsoldeClient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                mButtonModifierSoldeClient.setEnabled(s.toString().length() != 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mEditTextidClient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                mButtonModifierSoldeClient.setEnabled(s.toString().length() != 0);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //implementons le bouton modifiersolde client

        mButtonModifierSoldeClient.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //recuperation des donnees
                String idclientSaisi = mEditTextidClient.getText().toString();
                float soldeClientSaisi = Float.valueOf(mEditTextsoldeClient.getText().toString());

                CompteClient1 = new CompteClient();
                CompteClient1.setIdClient(idclientSaisi);
                CompteClient1.setSoldeClient(soldeClientSaisi);

                CompteClientManager1= new CompteClientManager(context);
                CompteClientManager1.open();// ouverture de la base de donne
                int numero = CompteClientManager1.modifierSoldeClient(CompteClient1);

                Toast.makeText(getBaseContext(),"le nombre de ligne affecté est "+numero,Toast.LENGTH_LONG).show();
                CompteClientManager1.close();
                finish();
            }
        });







    }

    public void QuitterModifierSoldeCompteClient(View view)
    {
        this.finish();
    }
}