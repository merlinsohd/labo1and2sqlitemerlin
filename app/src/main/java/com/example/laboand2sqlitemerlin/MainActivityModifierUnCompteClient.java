package com.example.laboand2sqlitemerlin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.laboand2sqlitemerlin.Manager.CompteClientManager;
import com.example.laboand2sqlitemerlin.Modeles.CompteClient;

import java.sql.Date;

public class MainActivityModifierUnCompteClient extends AppCompatActivity
{
    private EditText mEditTextidClient;
    private EditText mEditTextNomClient;
    private EditText mEditTextPrenomClient;
    private EditText mEditTextNomUtilisateurClient;
    private EditText mEditTextmotDePasseClient;
    private EditText mEditTextadresseclient;
    private EditText mEditTextsoldeClient;
    private EditText mEditTextcreditClient;
    private EditText mEditTextDateOuvertureCompte;
    private Button mButtonModifierUnCompteClient;

    private CompteClient CompteClient1; //objet compteclient
    private Context context = this; // context pour la base de donnee
    private CompteClientManager CompteClientManager1; // objet pour la connection à la base de donne



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_modifier_un_compte_client);

        //referencons les éléments graphiques
        mEditTextidClient = findViewById(R.id.EditTextidClient);
        mEditTextNomClient = findViewById(R.id.EditTextNomClient);
        mEditTextPrenomClient = findViewById(R.id.EditTextPrenomClient);
        mEditTextNomUtilisateurClient = findViewById(R.id.EditTextNomUtilisateurClient);
        mEditTextmotDePasseClient = findViewById(R.id.EditTextmotDePasseClient);
        mEditTextadresseclient = findViewById(R.id.EditTextadresseclient);
        mEditTextsoldeClient = findViewById(R.id.EditTextsoldeClient);
        mEditTextcreditClient = findViewById(R.id.EditTextcreditClient);
        mEditTextDateOuvertureCompte = findViewById(R.id.EditTextDateOuvertureCompte);

        mButtonModifierUnCompteClient = findViewById(R.id.ButtonModifierUnCompteClient);

        //desactivation du bouton

        mButtonModifierUnCompteClient.setEnabled(false);

        //activation du bouton

        mEditTextidClient.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                mButtonModifierUnCompteClient.setEnabled(s.toString().length() != 0);


            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        mEditTextNomClient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                mButtonModifierUnCompteClient.setEnabled(s.toString().length() != 0);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mEditTextPrenomClient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                mButtonModifierUnCompteClient.setEnabled(s.toString().length() != 0);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mEditTextNomUtilisateurClient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                mButtonModifierUnCompteClient.setEnabled(s.toString().length() != 0);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


         mEditTextmotDePasseClient.addTextChangedListener(new TextWatcher() {
             @Override
             public void beforeTextChanged(CharSequence s, int start, int count, int after)
             {

             }

             @Override
             public void onTextChanged(CharSequence s, int start, int before, int count)
             {
                 mButtonModifierUnCompteClient.setEnabled(s.toString().length() != 0);

             }

             @Override
             public void afterTextChanged(Editable s)
             {

             }
         });


         mEditTextadresseclient.addTextChangedListener(new TextWatcher() {
             @Override
             public void beforeTextChanged(CharSequence s, int start, int count, int after) {

             }

             @Override
             public void onTextChanged(CharSequence s, int start, int before, int count)
             {
                 mButtonModifierUnCompteClient.setEnabled(s.toString().length() != 0);

             }

             @Override
             public void afterTextChanged(Editable s) {

             }
         });

         mEditTextsoldeClient.addTextChangedListener(new TextWatcher() {
             @Override
             public void beforeTextChanged(CharSequence s, int start, int count, int after) {

             }

             @Override
             public void onTextChanged(CharSequence s, int start, int before, int count)
             {
                 mButtonModifierUnCompteClient.setEnabled(s.toString().length() != 0);

             }

             @Override
             public void afterTextChanged(Editable s) {

             }
         });

         mEditTextcreditClient.addTextChangedListener(new TextWatcher() {
             @Override
             public void beforeTextChanged(CharSequence s, int start, int count, int after) {

             }

             @Override
             public void onTextChanged(CharSequence s, int start, int before, int count)
             {
                 mButtonModifierUnCompteClient.setEnabled(s.toString().length() != 0);

             }

             @Override
             public void afterTextChanged(Editable s) {

             }
         });


         mEditTextDateOuvertureCompte.addTextChangedListener(new TextWatcher() {
             @Override
             public void beforeTextChanged(CharSequence s, int start, int count, int after) {

             }

             @Override
             public void onTextChanged(CharSequence s, int start, int before, int count)
             {
                 mButtonModifierUnCompteClient.setEnabled(s.toString().length() != 0);

             }

             @Override
             public void afterTextChanged(Editable s) {

             }
         });

         //implementation du bouton mButtonModifierUnCompteClient
        mButtonModifierUnCompteClient.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //recuperation des donnees
                //recuperation des donnees
                String idclientSaisi = mEditTextidClient.getText().toString();
                String nomclientSaisi = mEditTextNomClient.getText().toString();
                String prenomClientSaisi = mEditTextPrenomClient.getText().toString();
                String adresseClientSaisi = mEditTextmotDePasseClient.getText().toString();
                String nomUtilisateurSaisi = mEditTextNomUtilisateurClient.getText().toString();
                String motDepasseUtilisateurSaisi = mEditTextmotDePasseClient.getText().toString();
                float soldeClioentSaisi = Float.valueOf(mEditTextsoldeClient.getText().toString());
                float CreditClientSaisi = Float.valueOf(mEditTextcreditClient.getText().toString());
                Date DateouvertureSaisi = Date.valueOf(mEditTextDateOuvertureCompte.getText().toString());

                CompteClient1 = new CompteClient();
                CompteClient1.setIdClient(idclientSaisi);
                CompteClient1.setNomClient(nomclientSaisi);
                CompteClient1.setPrenomClient(prenomClientSaisi);
                CompteClient1.setAdresseClient(adresseClientSaisi);
                CompteClient1.setNomUtilisateurClient(nomUtilisateurSaisi);
                CompteClient1.setMotDePasseClient(motDepasseUtilisateurSaisi);
                CompteClient1.setSoldeClient(soldeClioentSaisi);
                CompteClient1.setCreditClient(CreditClientSaisi);
                CompteClient1.setDateOuvertureCompte(DateouvertureSaisi);


                CompteClientManager1 = new CompteClientManager(context);
                CompteClientManager1.open();
                int nombrLigneModifiees = CompteClientManager1.modifierInformationclient(CompteClient1);
                Toast.makeText(getBaseContext(),"le nombre de ligne modifié est "+nombrLigneModifiees,Toast.LENGTH_LONG).show();
                CompteClientManager1.close();
                finish();
            }
        });


    }

    public void QuitterModifierCompteClient(View view)
    {
        this.finish();
    }
}