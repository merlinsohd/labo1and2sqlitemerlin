package com.example.laboand2sqlitemerlin;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.laboand2sqlitemerlin.Manager.CompteClientManager;

/**
 * Created by Merlin Sohdjeukeu- Teccart 2020-09-30.
 */

//cette classe permet la gestion de la base de donne DBClient
public class GestionSQLite extends SQLiteOpenHelper
{
    private static final String nomDeBaseD = "BaseDCompteClient";
    private static final  int VersionDeLaBaseD = 1;


  // Le constructeur de cette

    public GestionSQLite(Context context)
    {
        super(context, nomDeBaseD, null, VersionDeLaBaseD);
    }

    // les deus methodes de cette classe
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        // Requete de creation de la table et de la base de donnee
//        db.execSQL(CompteClientManager.getCreation_TableClient());//creation de la table
//        Log.i("BaseDonne","oncreate invoked");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)    {
//        db.execSQL(CompteClientManager.getSuppressionTableClient());
//        Log.i("BaseDonne","onUpgrade");
    }
}
