package com.example.laboand2sqlitemerlin.Manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.laboand2sqlitemerlin.GestionSQLite;
import com.example.laboand2sqlitemerlin.Modeles.CompteClient;

import java.sql.Date;
import java.util.ArrayList;

/**
 * Created by Merlin Sohdjeukeu- Teccart 2020-09-30.
 */

//Cette classe permet la connexion à la base de donnee avec les methodes
public class CompteClientManager
{
    //La table CompteClient de la base de donnee
    public static final String nomdeLatableClient = "Client";
    public static final String colonneidClient = "idClient";
    public static  final String colonnenomClient ="nomClient";
    public static final String colonnePrenomClient = "PrenomClient";
    public static final String colonneadresseClient ="adresseClient";
    public static final String colonnenomUtilisateurClient = "nomUtilisateurClient";
    public static final  String colonnemotDePasseClient="motDePasseClient";
    public static final String colonnesoldeClient = "soldeClient";
    public static final  String colonnecreditClient ="creditClient" ;
    public static final String colonneDateOuvertureCompte = "DateOuvertureCompte"; // Date ouverture compte



    // Instruction SQL pour la creation de la table
    private static final String Creation_TableClient = "CREATE TABLE IF NOT EXISTS "
                                                       +nomdeLatableClient
                                                       +"("
                                                       +colonneidClient+" TEXT NOT NULL PRIMARY KEY,"
                                                       +colonnenomClient+" TEXT NOT NULL,"
                                                       +colonnePrenomClient+" TEXT NOT NULL,"
                                                       +colonneadresseClient+" TEXT NOT NULL,"
                                                       +colonnenomUtilisateurClient+" TEXT NOT NULL,"
                                                       +colonnemotDePasseClient+" TEXT NOT NULL,"
                                                       +colonnesoldeClient+" REAL NOT NULL,"
                                                       +colonnecreditClient+" REAL NOT NULL,"
                                                       +colonneDateOuvertureCompte+" TEXT NOT NULL"
                                                       +");";

    //Instruction sql pour la suppression de la table
    public static final String SuppressionTableClient = "DROP TABLE IF EXISTS "+nomdeLatableClient+";";

    //Gestion du fichier SQLite
    private GestionSQLite maBaseSQLite;
    private SQLiteDatabase BDclient;

    public static String getCreation_TableClient() {
        return Creation_TableClient;
    }

    public static String getSuppressionTableClient() {
        return SuppressionTableClient;
    }
    //Constructeur

    public CompteClientManager(Context context)
    {

        maBaseSQLite = new GestionSQLite(context);
    }


    //Les propriétés des attributs GestionSQLite/SQLiteDatabase

    public void open()
    {
        // ouverture de la base de donneesoit de la table en lecture et ecriture
        BDclient = maBaseSQLite.getWritableDatabase();
    }


    public void close()
    {
        //fermeture de la base de donne
        BDclient.close();
    }

    // Methode insertion dans la base de donne

    public long AjouterUnClient(CompteClient client)
    {
        //BDclient = maBaseSQLite.getWritableDatabase();
            // retourne le numero de ligne inseree
        ContentValues value = new ContentValues();
        value.put(colonneidClient,client.getIdClient());
        value.put(colonnenomClient,client.getNomClient());
        value.put(colonnePrenomClient,client.getPrenomClient());
        value.put(colonneadresseClient,client.getAdresseClient());
        value.put(colonnenomUtilisateurClient,client.getNomUtilisateurClient());
        value.put(colonnemotDePasseClient,client.getMotDePasseClient());
        value.put(colonnesoldeClient,client.getSoldeClient());
        value.put(colonnecreditClient,client.getCreditClient());
        value.put(colonneDateOuvertureCompte,client.getDateOuvertureCompte().toString());

        // insertion des donnees dans la base de donnee BDClient/tableClient
        return BDclient.insert(nomdeLatableClient,null,value);
    }

    // methode suppression d'un client dans la base de donne

    public int SuppressionClient(CompteClient client)
    {
        String where = colonneidClient+" = ?";
        String [] whereArgs = {client.getIdClient()+""};

        return BDclient.delete(nomdeLatableClient,where,whereArgs);
    }

    // modifier les informations d'un client
    public int modifierInformationclient(CompteClient client)
    {
        ContentValues value = new ContentValues();
        value.put(colonneidClient,client.getIdClient());
        value.put(colonnenomClient,client.getNomClient());
        value.put(colonnePrenomClient,client.getPrenomClient());
        value.put(colonneadresseClient,client.getAdresseClient());
        value.put(colonnenomUtilisateurClient,client.getNomUtilisateurClient());
        value.put(colonnemotDePasseClient,client.getMotDePasseClient());
        value.put(colonnesoldeClient,client.getSoldeClient());
        value.put(colonnecreditClient,client.getCreditClient());
        value.put(colonneDateOuvertureCompte,client.getDateOuvertureCompte().toString());

        String where = colonneidClient+" = ?";
        String[] whereArgs = {client.getIdClient()+""};

        return BDclient.update(nomdeLatableClient,value,where,whereArgs);
    }

    //obtenir les informations d'un client

    public ArrayList<CompteClient> fouirnirTousLesComptesClient(String idclient)
    {
        ArrayList<CompteClient> ListeCompteClients = new ArrayList<CompteClient>();

        //Cursor curseur = BDclient.rawQuery("SELECT * FROM "+nomdeLatableClient+" WHERE "+colonneidClient+" = "+)


        return ListeCompteClients;
    }






    //Modifier credit du client

    public int modifierCreditClient(CompteClient CompteClient1)
    {

            ContentValues value = new ContentValues();
            value.put(colonnecreditClient,CompteClient1.getCreditClient());

            String where = colonneidClient+" = ? ";
            String [] whereArgs = {CompteClient1.getIdClient()+""};
            return BDclient.update(nomdeLatableClient,value,where,whereArgs);
    }

    //Modifier le solsde du client
    public int modifierSoldeClient(CompteClient cptClient)
    {
        ContentValues value = new ContentValues();
        value.put(colonnesoldeClient,cptClient.getSoldeClient());

        String where = colonneidClient+" = ? ";
        String[] whereArgs = {cptClient.getIdClient()+""};

     return BDclient.update(nomdeLatableClient,value,where,whereArgs);
    }

    //Methode permettant d'obtenir les informations d'un compte client

    public ArrayList<CompteClient> getCompteClient(String idCompteClient)
    {

         CompteClient CompteClient1 = new CompteClient();
        String [] whereArgs = new String[]{idCompteClient};

       Cursor curseur = BDclient.rawQuery("SELECT * FROM "+nomdeLatableClient+" WHERE "+ colonneidClient+" = ?",whereArgs);

          if(curseur != null)
          {
              curseur.moveToFirst();
          }

           ArrayList<CompteClient> ListeCompte = new ArrayList<CompteClient>();

           while(!curseur.isAfterLast())
          {
              CompteClient1.setIdClient( curseur.getString(0));
              CompteClient1.setNomClient(curseur.getString(1));
              CompteClient1.setPrenomClient(curseur.getString(2));
              CompteClient1.setAdresseClient(curseur.getString(3));
              CompteClient1.setNomUtilisateurClient(curseur.getString(4));
              CompteClient1.setMotDePasseClient(curseur.getString(5));
              CompteClient1.setSoldeClient(curseur.getFloat(6));
              CompteClient1.setCreditClient(curseur.getFloat(7));
              CompteClient1.setDateOuvertureCompte(java.sql.Date.valueOf(curseur.getString(8)));

            // inserons les valeurs recuperées dans l'objet vide
            ListeCompte.add(CompteClient1);
             curseur.moveToNext();
          }
        return ListeCompte;
    }

}
