package com.example.laboand2sqlitemerlin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.laboand2sqlitemerlin.Manager.CompteClientManager;
import com.example.laboand2sqlitemerlin.Modeles.CompteClient;

public class MainActivityEffacerUnCompteClient extends AppCompatActivity
{
    private Button mButtonEffacerUnCompteClient;
    private EditText mEditTextEffaceridClient;
    private Context context = this;
    private CompteClientManager CompteClientManager1;
    private CompteClient CompteClient1;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_effacer_un_compte_client);

        mButtonEffacerUnCompteClient = findViewById(R.id.ButtonEffacerUnCompteClient);
        mEditTextEffaceridClient = findViewById(R.id.EditTextEffaceridClient);

        //desactver le bouton au chargement de la page
                mButtonEffacerUnCompteClient.setEnabled(false);

        //Activation du bouton effacer
        mEditTextEffaceridClient.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                mButtonEffacerUnCompteClient.setEnabled(s.toString().length() != 0);
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

         // implementation du bouton Supprimer un compte
        mButtonEffacerUnCompteClient.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // recuperation de la cle primaire du compte a supprimer
                String idclientAeffecerSaisi = mEditTextEffaceridClient.getText().toString();

                CompteClient1 = new CompteClient();// creation d'un objet CompteClient
                CompteClient1.setIdClient(idclientAeffecerSaisi);

                CompteClientManager1 = new CompteClientManager(context);//connection à la base
                CompteClientManager1.open();// ouverture  de la table en lecture et ecriture
                int nombreDelaLignesupprimer = CompteClientManager1.SuppressionClient(CompteClient1);

                Toast.makeText(getBaseContext(),"le nombre de ligne supprimées est "+nombreDelaLignesupprimer,Toast.LENGTH_LONG).show();
                CompteClientManager1.close();
                finish();
               }
        });


    }



    public void QuitterEffacerCompteClient(View view)
    {
        this.finish();
    }
}